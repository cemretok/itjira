package itjira;

import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AssignToMeTSTENV {
	public void assignToMe(String filter, Statement stmt, String username, String password, String sResult) {
		try {
			String exePath = null;
			FindPathWithOSName findOS = new FindPathWithOSName();
			exePath = findOS.findOsName();
			System.setProperty("webdriver.chrome.driver", exePath);
			WebDriver driver = new ChromeDriver();
			driver.get("https://itsupport.vodafone.com.tr/browse/" + sResult + "?filter=" + filter + "&os_username="
					+ username + "&os_password=" + password);
			
			WebElement defStatus = driver.findElement(By.xpath("//*[@id='internal-520019_10010']/div[1]/ul/li[2]/span"));
			
			String defStatu = defStatus.getAttribute("jira-issue-status-tooltip-title");
			
			System.out.println(defStatu);
			
			if (defStatu.equals("CLOSED")){		
			
			WebElement assignButton = driver.findElement(By.xpath("//*[@id='assign-issue']"));
			assignButton.click();

			WebDriverWait wait = new WebDriverWait(driver, 10);

			WebElement fillUsername = wait
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='assignee-field']")));

			fillUsername.sendKeys(username);

			WebElement assignClick = driver.findElement(By.xpath("//*[@id='assign-issue-submit']"));
			assignClick.click();
			
			driver.close();
			//driver.quit();
			
			}
		} catch (Exception e) {
			System.out.println("assignToMe " + e.getMessage());
		}
	}

	public void closeTSTENV(String filter, Statement stmt, String username, String password, String sResult) {
		try {
			String exePath = null;
			FindPathWithOSName findOS = new FindPathWithOSName();
			exePath = findOS.findOsName();
			System.setProperty("webdriver.chrome.driver", exePath);
			WebDriver driver = new ChromeDriver();
			driver.get("https://itsupport.vodafone.com.tr/browse/" + sResult + "?filter=" + filter + "&os_username="
					+ username + "&os_password=" + password);

			// TODO

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
