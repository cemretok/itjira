package itjira;

import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CheckDueTime {

	public void checkDueTime(String sDevDueDate, String filter, Statement stmt, String username, String password,
			String sResult) {

		SendMail sm = new SendMail();
		try {
			FillDevDueDate fddd = new FillDevDueDate();
			if (sDevDueDate.trim().equals("NULL") || sDevDueDate.trim().equals(null) || sDevDueDate.trim().equals("")) {
				sDevDueDate = "01.01.1990";
			}
			Calendar today = Calendar.getInstance();
			SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
			Date cDate = format.parse(sDevDueDate);
			Date dNow = new Date();

			dNow = today.getTime();
			if (sDevDueDate != "NULL") {
				cDate = format.parse(sDevDueDate);
			}

			if (cDate.before(dNow) || sDevDueDate == "NULL") {
				DateFormat formatter = new SimpleDateFormat("dd/MMM/yy", Locale.ENGLISH);
				today.add(Calendar.MONTH, 1);
				dNow = today.getTime();
				String newDate = formatter.format(dNow);
				System.out.println(newDate);
				fddd.fillDevDueDate(sDevDueDate, filter, stmt, username, password, sResult, newDate);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			sm.sendMailError("checkDueTime : " + " defect id :" + sResult + " " + e.toString());
		}

	}

}
