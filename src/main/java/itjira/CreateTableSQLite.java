package itjira;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreateTableSQLite {

	public Statement createTable(String[] jiraHeaders, int headerColumnNo, String filter) {
		String tableName = "TEMP_DEFT_";
		String sTempDb = "kafein.db";
		String sJdbc = "jdbc:sqlite";
		String sDbUrl = sJdbc + ":" + sTempDb;
		int iTimeout = 30;
		boolean tableExists;
		SendMail sm = new SendMail();

		Statement stmt = null;

		try {
			String sDriverName = "org.sqlite.JDBC";
			Class.forName(sDriverName);
			Connection conn = DriverManager.getConnection(sDbUrl);
			stmt = conn.createStatement();

			String checkTable = "SELECT * FROM " + tableName + filter;
			stmt.executeQuery(checkTable);
			tableExists = true;
		} catch (Exception e) {
			tableExists = e.getMessage().contains("no such table: " + tableName);
			int columnCount = 1;
			if (tableExists) { // if table does not exists
				String sMakeTableLog = "CREATE TABLE " + tableName + filter + " ( ";
				for (int i = 1; i < headerColumnNo; i++) {					
					if (columnCount <= 5
							&& (jiraHeaders[i].equals("Key")  || jiraHeaders[i].equals("Summary") || jiraHeaders[i].equals("Assignee")
									|| jiraHeaders[i].equals("Status") || jiraHeaders[i].equals("Due"))) { 
						sMakeTableLog = sMakeTableLog + jiraHeaders[i] + " TEXT, ";
						columnCount++;
						//System.out.println("jiraHeaders[i] : " + i +" : " + jiraHeaders[i] + " GIRDI");
					} /*else if (columnCount == 5) {
						sMakeTableLog = sMakeTableLog + jiraHeaders[i] + " TEXT ";
						columnCount++;
					}*/
				}
				sMakeTableLog = sMakeTableLog + "INSERT_FLAG TEXT DEFAULT '0', DELETE_FLAG TEXT DEFAULT '0')";
				try {
					stmt.executeUpdate(sMakeTableLog);
					// System.out.println("Table created : DEFECT_LOG_"+filter
					// );
				} catch (Exception e1) {
					System.out.println(e1.getMessage());
					sm.sendMailError("createTable: " + e.toString());
				}
			}

		}
		return stmt;
	}

}