package itjira;

import java.sql.ResultSet;
import java.sql.Statement;

public class DeleteTableDecreaseDef {
	public String deleteDecreaseDef(String username, String password, String filter, Statement stmt) {
		String tableName = "TEMP_DEFT_";
		String decreaseBody = "";
		String increaseBody = "";
		String fixingBugs = "";
		String diBody = "";
		try {
			String sMakeSelect = "SELECT COUNT(Key) from " + tableName + filter + " WHERE DELETE_FLAG = '0'";
			ResultSet rs = stmt.executeQuery(sMakeSelect);
			String sResult = rs.getString("COUNT(Key)");
			// System.out.println("Azalan Defect Sayisi :" + sResult);
			IncreaseDecreaseDefects dd = new IncreaseDecreaseDefects();
			decreaseBody = dd.decreaseDefects(username, password, filter, stmt);

			String sMakeSelect2 = "SELECT COUNT(Key) from " + tableName + filter
					+ " WHERE INSERT_FLAG='0' AND DELETE_FLAG = '1'";
			ResultSet rs2 = stmt.executeQuery(sMakeSelect2);
			String sResult2 = rs2.getString("COUNT(Key)");
			// System.out.println("Artan defect sayisi =" + sResult2);

			increaseBody = dd.increasedDefects(filter, stmt);

			
			String deleteTable = "DELETE FROM " + tableName + filter + " WHERE DELETE_FLAG = '0'";
			stmt.executeUpdate(deleteTable);

			String sMakeSelect3 = "SELECT COUNT(Key) from " + tableName + filter;
			ResultSet rs3 = stmt.executeQuery(sMakeSelect3);
			String sResult3 = rs3.getString("COUNT(Key)");
			// System.out.println("Artan defect sayisi =" + sResult2);
			String totalDefectsCount = "Toplam Defect Sayisi/ Total Defect Count is :" + sResult3;

			fixingBugs = dd.FixingBugDefects(filter, stmt);
			diBody = decreaseBody + "\n" + increaseBody + "\n"+ "Fixing Bug and Workaround Fixing Defects are: "+"\n" + fixingBugs + "\n" + totalDefectsCount;

		} catch (Exception e) {
			System.out.println("deleteDecreaseDef " + e.getMessage());
		}
		return diBody;
	}
}
