package itjira;

import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DoWAResolved {
	public void doWAResolved(String filter, Statement stmt, String username, String password, String sResult,
			String sAssignee) {
		SendMail sm = new SendMail();
		try {

			String exePath = null;
			FindPathWithOSName findOS = new FindPathWithOSName();
			exePath = findOS.findOsName();
			System.setProperty("webdriver.chrome.driver", exePath);
			ChromeOptions options = new ChromeOptions();
			
			
			options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
			options.addArguments("--no-sandbox"); // Bypass OS security model
			options.addArguments("--headless"); // Bypass OS security model
			options.addArguments("--disable-extensions");
			options.addArguments("--disable-gpu");
			
			
			
			WebDriver driver = new ChromeDriver(options);
			driver.get("https://itsupport.vodafone.com.tr/browse/" + sResult + "?filter=" + filter + "&os_username="
					+ username + "&os_password=" + password);
			driver.manage().window().setPosition(new Point(0, 0));
			driver.manage().window().setSize(new Dimension(1200, 768));
			WebElement Priorty;
			
			Actions action = new Actions(driver);
			
			try {
			Priorty = driver.findElement(By.xpath("//*[@id='priority-val']/img"));
			}
			catch (Exception e) {
				try {
				Priorty = driver.findElement(By.xpath("//*[@id=\'priority-val\']/text()"));
				}
				catch (Exception ex) {
					Priorty = driver.findElement(By.xpath("//*[@id=\'priority-val\']"));					
				}
			}
			
			String title = Priorty.getAttribute("title");
			
			
			title = title.substring(0, 1);
			int delay = 45;

			if (title.equalsIgnoreCase("l")) {
				delay = delay - 120 * 60;
			}
			if (title.equalsIgnoreCase("m")) {
				delay = delay - 96 * 60;
			}
			if (title.equalsIgnoreCase("h")) {
				delay = delay - 4 * 60;
			}
			if (title.equalsIgnoreCase("c")) {
				delay = delay - 3 * 60;
			}

			WebElement Created = driver.findElement(By.xpath("//*[@id='created-val']/time"));
			

			String creationDate = Created.getAttribute("datetime");

			creationDate = creationDate.substring(0, creationDate.lastIndexOf("+"));
			creationDate = creationDate.replace("T", " ");
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date cDate = format.parse(creationDate);
			Date dNow = new Date();
			Calendar today = Calendar.getInstance();
			System.out.println(dNow);
			today.add(Calendar.MINUTE, delay);
			dNow = today.getTime();
			System.out.println("cDate :" + cDate);
			System.out.println("added dNow :" + dNow);

			WebDriverWait wait = new WebDriverWait(driver, 10);

			String assignUser = username.substring(4, username.length()); // ibm + first ch =4, it should be contains
																			// only surname
			boolean exit = false;

			if (cDate.before(dNow)) {

				for (int i = 0; i < assignUser.length(); i++) {
					if (assignUser.charAt(i) == '1' || assignUser.charAt(i) == '2' || assignUser.charAt(i) == '3'
							|| assignUser.charAt(i) == '4' || assignUser.charAt(i) == '5' || assignUser.charAt(i) == '6'
							|| assignUser.charAt(i) == '7' || assignUser.charAt(i) == '8' || assignUser.charAt(i) == '9'
							|| assignUser.charAt(i) == '0') {
						assignUser = assignUser.substring(0, i);
						i = assignUser.length();
					}
				}
				
				String sAssigneeUpd = "";
				
				for (int j = 0; j < sAssignee.length(); j++) {
					if(sAssignee.charAt(j)==',') {
						sAssigneeUpd = sAssignee.substring(0,j);
						j=sAssignee.length();
					}
				}
				
				System.out.println(sAssigneeUpd+ " -- "+ assignUser);
				if (!assignUser.toLowerCase().contains(sAssigneeUpd.toLowerCase())) {
					WebElement assignToMe = driver.findElement(By.xpath("//*[@id='action_id_3171']/span"));
					assignToMe.click();
					WebElement waelement = wait
							.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='customfield_22770']")));
					
					WebElement elementAssing =driver.findElement(By.xpath("//*[@id='customfield_22770']"));
					action.doubleClick(elementAssing).perform();
					waelement.sendKeys(username);

					WebElement sendResolve = driver
							.findElement(By.xpath("//*[@id='issue-workflow-transition-submit']"));
					sendResolve.click();

				}
				WebElement workFlow = wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='opsbar-transitions_more']/span")));
				workFlow.click();

				WebElement resolve = wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//*[@id=\'action_id_3001\']/a/span")));
				resolve.click();

				WebElement WAResolution = wait.until(
						ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='customfield_23370']/option[8]")));
				WAResolution.click();

				WebElement DoWaFixing = driver.findElement(By.xpath("//*[@id='issue-workflow-transition-submit']"));
				DoWaFixing.click();

			} else {
				System.out.println("hen�z zaman� gelmedi");
			}

			driver.close();
			//driver.quit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			sm.sendMailError("doWAResolved : " + " defect id :" + sResult + " " + e.toString());
		}
	}
}
