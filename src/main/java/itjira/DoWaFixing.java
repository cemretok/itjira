package itjira;

import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.steadystate.css.parser.ParseException;

public class DoWaFixing {

	public void doWaFixing(String filter, Statement stmt, String username, String password, String sResult)
			throws ParseException {
		SendMail sm = new SendMail();
		try {

			String exePath = null;
			FindPathWithOSName findOS = new FindPathWithOSName();
			exePath = findOS.findOsName();
			
			
			System.setProperty("webdriver.chrome.driver", exePath);
			ChromeOptions options = new ChromeOptions();			
			
			options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
			options.addArguments("--no-sandbox"); // Bypass OS security model
			options.addArguments("--headless"); // Bypass OS security model
			options.addArguments("--disable-extensions");
			options.addArguments("--disable-gpu");
			
			
			
			WebDriver driver = new ChromeDriver(options);
			
			driver.get("https://itsupport.vodafone.com.tr/browse/" + sResult + "?filter=" + filter + "&os_username="
					+ username + "&os_password=" + password);
			driver.manage().window().maximize();

			WebDriverWait wait = new WebDriverWait(driver, 10);


				WebElement WAFixingButton = wait.until(ExpectedConditions
//						.elementToBeClickable(By.xpath("//*[@id='opsbar-opsbar-transitions']/li[1]/a")));
						.elementToBeClickable(By.xpath("//*[@id=\'action_id_2931\']/span")));
				WAFixingButton.click();

				WebElement waelement = wait
						.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='customfield_22770']")));

				waelement.sendKeys(username);

				WebElement sendResolve = wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath("//*[@id='issue-workflow-transition-submit']")));
				sendResolve.click();

			driver.close();
		} catch (Exception e) {
			System.out.println(e.toString());
			sm.sendMailError("doWaFixing :" + " defect id :" + sResult + " " + "filter " + filter + "  "+  e.toString());

		}

	}

}
