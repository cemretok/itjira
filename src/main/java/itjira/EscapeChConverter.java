package itjira;

public class EscapeChConverter {
	public String escapeChConverter(String data){
		data = data.replace("'", "");
		data = data.replace("\b", "");
		data = data.replace("\n", "");
		data = data.replace("\t", "");
		data = data.replace("\r", "");
		data = data.replace("\f", "");
		data = data.replace("\"", "");
		data = data.replace("\\", "");
		
		return data;
	}
}
