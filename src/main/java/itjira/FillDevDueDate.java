package itjira;

import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FillDevDueDate {

	public void fillDevDueDate(String sDevDueDate, String filter, Statement stmt, String username, String password,
			String sResult, String newDate) {
		SendMail sm = new SendMail();
		try {

			String exePath = null;
			FindPathWithOSName findOS = new FindPathWithOSName();
			exePath = findOS.findOsName();
			System.setProperty("webdriver.chrome.driver", exePath);
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
			options.addArguments("--no-sandbox"); // Bypass OS security model
			options.addArguments("--headless"); // Bypass OS security model
			options.addArguments("--disable-extensions");
			options.addArguments("--disable-gpu");
			WebDriver driver = new ChromeDriver(options);
			driver.get("https://itsupport.vodafone.com.tr/browse/" + sResult + "?filter=" + filter + "&os_username="
					+ username + "&os_password=" + password);
			driver.manage().window().setPosition(new Point(0, 0));
			driver.manage().window().setSize(new Dimension(1200, 768));
			WebDriverWait wait = new WebDriverWait(driver, 30);

			WebElement workFlow = wait.until(
					ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='opsbar-transitions_more']/span")));
			workFlow.click();

			WebElement deploymentEdit = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\'action_id_3401\']/span")));
			deploymentEdit.click();

			WebElement dueDate = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\'duedate\']")));
			dueDate.sendKeys(newDate);

			WebElement deploymentEditButtom = wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath("//*[@id=\'issue-workflow-transition-submit\']")));
			deploymentEditButtom.click();

			driver.close();
			//driver.quit();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			sm.sendMailError("fillDevDueDate : " + " defect id :" + sResult + " " + e.toString());

		}

	}

}
