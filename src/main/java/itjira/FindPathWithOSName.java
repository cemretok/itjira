package itjira;

import java.io.File;

public class FindPathWithOSName {
	private static String OS = System.getProperty("os.name").toLowerCase();

	public String findOsName(){
		
		String exePath = null;
		if (isWindows()) {
			//System.out.println("This is Windows");
			exePath = System.getProperty("user.dir")+File.separator+"/selenium/chromedriver.exe";
		} else if (isMac()) {
			//System.out.println("This is Mac");
		} else if (isUnix()) {
			//System.out.println("This is Unix or Linux");
			exePath = System.getProperty("user.dir")+File.separator+"/selenium/chromedriver";
		} else if (isSolaris()) {
			//System.out.println("This is Solaris");
		} else {
			System.out.println("Your OS is not support!!");
		}
		
		return exePath;
	}
	public static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	public static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	public static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	public static boolean isSolaris() {
		return (OS.indexOf("sunos") >= 0);
	}

	public static String getOS() {
		if (isWindows()) {
			return "win";
		} else if (isMac()) {
			return "osx";
		} else if (isUnix()) {
			return "uni";
		} else if (isSolaris()) {
			return "sol";
		} else {
			return "err";
		}
	}
}
