package itjira;

import java.sql.ResultSet;
import java.sql.Statement;

public class IncreaseDecreaseDefects {
	public String decreaseDefects(String username, String password, String filter, Statement stmt) {
		String tableName = "TEMP_DEFT_";
		String decreaseBody = "";
		int dcount = 1;
		SendMail sm = new SendMail();

		try {
			String selectDecreases = "SELECT Summary, Key from " + tableName + filter + " WHERE DELETE_FLAG = '0'";
			ResultSet rs = stmt.executeQuery(selectDecreases);
			while (rs.next()) {
				String sResult = rs.getString("Summary");
				String defectId = rs.getString("Key");
				// System.out.println("Azalan Defect : " + sResult);
				decreaseBody = decreaseBody + dcount + ". Azalan Defect / Decrease Defect : " + defectId + " -- "
						+ sResult + "\n";
				decreaseBody = decreaseBody + "\n";
				dcount++;
			}
		} catch (Exception e) {
			System.out.println("decreaseDefects" + e.getMessage());
			sm.sendMailError("decreaseDefects" + e.toString());
		}
		return decreaseBody;
	}

	public String increasedDefects(String filter, Statement stmt) {
		String tableName = "TEMP_DEFT_";
		String increaseBody = "";
		int icount = 1;
		SendMail sm = new SendMail();

		try {
			String selectIncrease = "SELECT Summary, Key from " + tableName + filter
					+ " WHERE INSERT_FLAG='0' AND DELETE_FLAG = '1'";
			ResultSet rs1 = stmt.executeQuery(selectIncrease);
			while (rs1.next()) {
				String sResult1 = rs1.getString("Summary");
				String sResult2 = rs1.getString("Key");
				// System.out.println("Artan Defect : " + sResult1);
				increaseBody = increaseBody + icount + ". Artan Defect/ Increase Defect : " + sResult2 + " -- "
						+ sResult1 + "\n";
				increaseBody = increaseBody + "\n";
				icount++;
			}
		} catch (Exception e) {
			System.out.println("increasedDefects" + e.getMessage());
			sm.sendMailError("increasedDefects" + e.toString());
		}
		return increaseBody;
	}
	
	public String FixingBugDefects(String filter, Statement stmt) {
		String tableName = "TEMP_DEFT_";
		String fixingBody = "";
		int icount = 1;
		SendMail sm = new SendMail();

		try {		
			
			String selectFixing = "select Summary, Key from " + tableName + filter
					+ " where upper(status) in ('FIXING BUG','WORKAROUND FIXING','OPEN','WAITING INFO- DEV')";
			ResultSet rs1 = stmt.executeQuery(selectFixing);
			while (rs1.next()) {
				String sResult1 = rs1.getString("Summary");
				String sResult2 = rs1.getString("Key");
				// System.out.println("Artan Defect : " + sResult1);
				fixingBody = fixingBody + icount + "- " + sResult2 + " -- "
						+ sResult1 + "\n";
				fixingBody = fixingBody + "\n";
				icount++;
			}
		} catch (Exception e) {
			System.out.println("FixingBugDefects" + e.getMessage());
			sm.sendMailError("FixingBugDefects" + e.toString());
		}
		return fixingBody;
	}
}
