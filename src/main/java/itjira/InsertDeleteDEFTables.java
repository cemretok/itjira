package itjira;

import java.sql.Statement;

public class InsertDeleteDEFTables {
	public void insertDeleteDiff(String[][] dataSet, int i, int headerColumnNo, String filter, Statement stmt,
			int defColumn, int update_icolumn, int update_dcolumn, int[] usingCounts, int dueColumn, int assigneeColumn,
			int summaryColumn, int statusColumn) {

		DeleteDiffDefects(dataSet, i, defColumn, filter, stmt, defColumn, update_icolumn, update_dcolumn, usingCounts,
				dueColumn, assigneeColumn, summaryColumn, statusColumn);
		insertDiffDefects(dataSet, i, headerColumnNo, filter, stmt, defColumn, update_icolumn, update_dcolumn,
				usingCounts, dueColumn, assigneeColumn, summaryColumn, statusColumn);

	}

	public void insertDiffDefects(String[][] dataSet, int i, int headerColumnNo, String filter, Statement stmt,
			int defColumn, int update_icolumn, int update_dcolumn, int[] usingCounts, int dueColumn, int assigneeColumn,
			int summaryColumn, int statusColumn) {
		String tableName = "TEMP_DEFT_";
		EscapeChConverter ecc = new EscapeChConverter();
		try {
			// BODY kismi yaratilir.
			String insertHeader = "INSERT INTO " + tableName + filter + " values( ";
			String insertBody = "";
			boolean usingColumn = false;
			int usingColumnCount = 0;

			for (int j = 1; j < headerColumnNo; j++) {
				// System.out.println("once - " + dataSet[i][j]);
				dataSet[i][j] = ecc.escapeChConverter(dataSet[i][j]);
				// System.out.println(dataSet[i][j]);
				if (dataSet[i][j].equals(null) || (dataSet[i][j].trim()).equals("")) {
					dataSet[i][j] = "NULL";
				}

				for (int j2 = 0; j2 < 5; j2++) {
					if (usingCounts[j2] == j) {
						usingColumn = true;
						usingColumnCount++;
					}
				}
				if (usingColumnCount == 5) {
					insertBody = insertBody + "'" + dataSet[i][j] + "'";
					usingColumnCount++;
				} else if (usingColumn && usingColumnCount < 5) {
					insertBody = insertBody + "'" + dataSet[i][j] + "', ";
				}

				usingColumn = false;
			}
			String insertTable = insertHeader + insertBody + ",'0','0')";

			// System.out.println(insertTable);
			stmt.executeUpdate(insertTable);

		} catch (Exception e) {
			System.out.println("insertDiffDefects " + e.getMessage());
			SendMail sm = new SendMail();
			sm.sendMailError("insertDiffDefects " + e.toString());
		}

		try {
			if (update_icolumn == 1) {
				String checkData = dataSet[i][defColumn];
				// System.out.println("update_icolumn " + dataSet[i][defColumn]);
				String sUpdateTable = "UPDATE " + tableName + filter
						+ " SET INSERT_FLAG ='1', DELETE_FLAG ='0' WHERE Key = '" + checkData + "'";
				stmt.executeUpdate(sUpdateTable);
				// System.out.println(sUpdateTable);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			if (update_dcolumn == 1) {
				String checkData = dataSet[i][defColumn];

				// System.out.println("update_dcolumn " + dataSet[i][defColumn]);
				String sUpdateTable = "UPDATE " + tableName + filter + " SET DELETE_FLAG ='1' WHERE Key = '" + checkData
						+ "'";
				stmt.executeUpdate(sUpdateTable);

				// System.out.println(sUpdateTable);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void DeleteDiffDefects(String[][] dataSet, int i, int headerColumnNo, String filter, Statement stmt,
			int defColumn, int update_icolumn, int update_dcolumn, int[] usingCounts, int dueColumn, int assigneeColumn,
			int summaryColumn, int statusColumn) {
		String tableName = "TEMP_DEFT_";
		String checkData = dataSet[i][defColumn];
		EscapeChConverter ecc = new EscapeChConverter();
		checkData = ecc.escapeChConverter(checkData);
		dataSet[i][summaryColumn] = ecc.escapeChConverter(dataSet[i][summaryColumn]);

		try {
			String deleteTable = "UPDATE " + tableName + filter + " SET Due = '" + dataSet[i][dueColumn]
					+ "' ,Status = '" + dataSet[i][statusColumn] + "' ,Assignee = '" + dataSet[i][assigneeColumn]
					+ "' ,Summary = '" + dataSet[i][summaryColumn] + "' ,DELETE_FLAG = '1', INSERT_FLAG='1', Key = '"
					+ dataSet[i][defColumn] + "' " + " WHERE Key = '" + dataSet[i][defColumn] + "'";
			stmt.executeUpdate(deleteTable);

		} catch (Exception e) {
			System.out.println("DeleteDiffDefects " + e.getMessage());
		}

	}
}
