package itjira;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSonParser {

	String username;
	String password;
	String domain;
	String from;
	String to;
	String filterId;
	String type;
	int count;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void getUserInfo(String _filter) { //
		String content;
		JSONParser parser = new JSONParser();
		try {
			count = 1;
			content = new String(Files.readAllBytes(Paths.get(System.getProperty("user.dir") + "/json/config.json")));

			Object obj = parser.parse(content.trim());

			JSONObject jsonObject = (JSONObject) obj;

			Iterator<JSONObject> iterator = jsonObject.values().iterator();

			String[] username = new String[jsonObject.size()];
			String[] password = new String[jsonObject.size()];
			String[] filterId = new String[jsonObject.size()];
			String[] domain = new String[jsonObject.size()];
			String[] from = new String[jsonObject.size()];
			String[] to = new String[jsonObject.size()];
			String[] type = new String[jsonObject.size()];
			String[] tstenvFilter = new String[jsonObject.size()];

			int i = 0;
			while (iterator.hasNext()) {
				JSONObject jsonChildObject = iterator.next();

				username[i] = (String) jsonChildObject.get("username");
				password[i] = (String) jsonChildObject.get("password");
				filterId[i] = (String) jsonChildObject.get("filterId");
				domain[i] = (String) jsonChildObject.get("domain");
				from[i] = (String) jsonChildObject.get("from");
				to[i] = (String) jsonChildObject.get("to");
				type[i] = (String) jsonChildObject.get("type");
				/*
				 * to[i] = to[i].replace(",", ";"); to[i] = to[i].replace("[",
				 * ""); to[i] = to[i].replace("]", ""); to[i] =
				 * to[i].replace("\"", "");
				 */
				if (_filter.equals(filterId[i])) {
					setUsername(username[i]);
					setPassword(password[i]);
					setDomain(domain[i]);
					setFrom(from[i]);
					setTo(to[i]);
					setType(type[i]);
					setFilter(filterId[i]);
					count++;
				}
				i++;

			}

		} catch (IOException e1) {
			System.out.println(e1.toString());
			SendMail sm = new SendMail();
			sm.sendMailError(" main :" + e1.toString());
		} catch (ParseException e) {
			System.out.println(e.toString());
			SendMail sm = new SendMail();
			sm.sendMailError(" main :" + e.toString());
			e.printStackTrace();
		}
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFilterId() {
		return filterId;
	}

	public void setFilter(String filterId) {
		this.filterId = filterId;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
