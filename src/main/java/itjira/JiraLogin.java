package itjira;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class JiraLogin {
	int rows_count;
	int columns_count;
	int headerColumn_count;
	String[] jiraHeaders = new String[headerColumn_count];
	String[][] dataSet = new String[rows_count][columns_count];
	int[] usingColumns = new int[5];
	boolean gotError = false;
	String URLError;
	String logOutCookie;

	public void connectJira(String username, String password, String filter) {
		try {

			String exePath = null;
			FindPathWithOSName findOS = new FindPathWithOSName();
			exePath = findOS.findOsName();
			System.setProperty("webdriver.chrome.driver", exePath);
			ChromeOptions options = new ChromeOptions();
			
			
			options.addArguments("--disable-dev-shm-usage"); // overcome limited
			options.addArguments("--no-sandbox"); // Bypass OS security model
			options.addArguments("--headless"); // Bypass OS security model
			options.addArguments("--disable-extensions");
			options.addArguments("--disable-gpu");
			options.addArguments("--incognito");
			
			
			
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			
			capabilities.setCapability("chrome.switches", Arrays.asList("--disable-extensions"));
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			
			
			int row = 0;			
			int startIndex =0;
			int totalIndexCount = 0;
			boolean temp = true;
			int k = 0;
			int usingCount = 0;

			
			WebDriver driver;
			
			do {
			driver = new ChromeDriver(options);
			
			String path = "https://itsupport.vodafone.com.tr/issues/?filter=" + filter + "&os_username=" + username
					+ "&os_password=" + password +"&startIndex="+ startIndex;
			System.out.println(filter +" path = " +path);
			driver.get("https://itsupport.vodafone.com.tr/issues/?filter=" + filter + "&os_username=" + username
					+ "&os_password=" + password +"&startIndex="+ startIndex);
			driver.manage().window().maximize();
			

			WebDriverWait wait = new WebDriverWait(driver, 10);
			
			URLError = driver.getCurrentUrl();
			
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	        FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir")+File.separator+"/selenium/jira_login.png"));
	        
	        logOutCookie = driver.manage().getCookieNamed("atlassian.xsrf.token").getValue();
	        
	        
	        WebElement totalIndex = wait.until(ExpectedConditions
					.elementToBeClickable(driver.findElement(By.xpath("//*[@id='content']/div[1]/div[3]/div/div/div/div/div/div/div[1]/div[1]/span/span[3]"))));
	        

			
	        
	        String stotal = totalIndex.getText();
	        
	        
	        totalIndexCount = Integer.parseInt(stotal);	        
	        
			/*
			 * START $1 Burada kolon isimleri tutuluyor Key, Summary, Assignee vb.
			 */
	        
			WebElement headers = driver.findElement(By.xpath("//*[@id='issuetable']/thead"));
			List<WebElement> headersColumn = headers.findElements(By.tagName("th"));
			int headerColumn_count = headersColumn.size();

			String[] jiraHeaders = new String[headerColumn_count];
			

			
			
			//setHeaderColumn_count(headerColumn_count);
			//setJiraHeaders(jiraHeaders);

			/*
			 * END $1
			 */

			/*
			 * START $2 Sat�r ve s�t�n de�erleri �ekilip 2 boyutlu diziye at�l�r.
			 *
			 */

			WebElement defects = driver.findElement(By.xpath("//*[@id='issuetable']/tbody"));

			List<WebElement> rows_defects = defects.findElements(By.tagName("tr"));
			int rows_count = rows_defects.size();

			List<WebElement> column_row = rows_defects.get(0).findElements(By.tagName("td"));
			int columns_count = column_row.size();
			//dataSet = new String[rows_count][columns_count];
			
			if(temp) {
			dataSet = new String[totalIndexCount][columns_count];
			temp = false;
			for ( ;k < headerColumn_count; k++) {
				jiraHeaders[k] = headersColumn.get(k).getText();
				if (jiraHeaders[k].equals("Key") || jiraHeaders[k].equals("Summary")
						|| jiraHeaders[k].equals("Assignee") || jiraHeaders[k].equals("Status")
						|| jiraHeaders[k].equals("Due")) {
					usingColumns[usingCount] = k;
					usingCount++;
				}

			}
			setJiraHeaders(jiraHeaders);

			setHeaderColumn_count(headerColumn_count);
			}
			
			int totalRowsCount =rows_count+startIndex;
			
			//rows_count yaz totalIndexCount yerine
			for (; row < totalRowsCount; row++) {
				column_row = rows_defects.get(row-startIndex).findElements(By.tagName("td"));
				int column_count = column_row.size();
				for (int column = 0; column < column_count; column++) {
					String celtext = column_row.get(column).getText();
					dataSet[row][column] = celtext;
				}
			}
			
			startIndex = startIndex + 50;
			
			//setHeaderColumn_count(headerColumn_count);
			//setJiraHeaders(jiraHeaders);
			//setDataSet(dataSet);

			//setRows_count(rows_count);
			//setColumns_count(columns_count);
			/*
			 * END $2
			 */
			driver.close();
			//driver.quit();
			
			}while(totalIndexCount>startIndex);
			
			
			setDataSet(dataSet);

			setRows_count(totalIndexCount);
			setColumns_count(columns_count);
			
			//driver.close();
			//driver.quit();
			
		} catch (Exception e) {
			System.out.println(e.toString());
			SendMail sm = new SendMail();
			sm.sendMailError(filter + " connectJira :" + URLError + " --- " + e.toString());
			gotError = true;

		}

	}
	
	public void disconnectJira(String username, String password, String filter) {
		
		try {
		String exePath = null;
		FindPathWithOSName findOS = new FindPathWithOSName();
		exePath = findOS.findOsName();
		System.setProperty("webdriver.chrome.driver", exePath);
		ChromeOptions options = new ChromeOptions();
		
		options.addArguments("--disable-dev-shm-usage"); // overcome limited
		options.addArguments("--no-sandbox"); // Bypass OS security model
		options.addArguments("--headless"); // Bypass OS security model
		options.addArguments("--disable-extensions");
		options.addArguments("--disable-gpu");
		
		WebDriver driver;
		driver = new ChromeDriver(options);
		driver.get("https://itsupport.vodafone.com.tr/secure/Logout!default.jspa?atl_token=" + logOutCookie);
		driver.manage().deleteAllCookies();
		

		driver.close();
		//driver.quit();
		
		}
		catch (Exception e) {
		System.out.println(e.toString());
		SendMail sm = new SendMail();
		sm.sendMailError(filter + " disconnectJira :" + e.toString());
	}
		
	}

	public String logOutCookie() {
		return logOutCookie;
	}

	public void setURLError(String logOutCookie) {
		logOutCookie = logOutCookie;
	}

	public int getRows_count() {
		return rows_count;
	}

	public void setRows_count(int rows_count) {
		this.rows_count = rows_count;
	}

	public int getColumns_count() {
		return columns_count;
	}

	public void setColumns_count(int columns_count) {
		this.columns_count = columns_count;
	}

	public int getHeaderColumn_count() {
		return headerColumn_count;
	}

	public void setHeaderColumn_count(int headerColumn_count) {
		this.headerColumn_count = headerColumn_count;
	}

	public String[] getJiraHeaders() {
		return jiraHeaders;
	}

	public void setJiraHeaders(String[] jiraHeaders) {
		this.jiraHeaders = jiraHeaders;
	}

	public String[][] getDataSet() {
		return dataSet;
	}

	public void setDataSet(String[][] dataSet) {
		this.dataSet = dataSet;
	}
	
	public boolean getGotError() {
		return gotError;
	}

	public void setGotError(boolean gotError) {
		this.gotError = gotError;
	}

	public int[] getUsingColumns() {
		return usingColumns;
	}

	public void setUsingColumns(int[] usingColumns) {
		this.usingColumns = usingColumns;
	}

}
