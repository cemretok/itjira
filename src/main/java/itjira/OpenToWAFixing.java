package itjira;

import java.sql.ResultSet;
import java.sql.Statement;

public class OpenToWAFixing {
	public void openToWA(String filter, Statement stmt, String username, String password) {
		String tableName = "TEMP_DEFT_";
		DoWaFixing dwaf = new DoWaFixing();
		DoWAResolved dwar = new DoWAResolved();
		SendMail sm = new SendMail();
		doWaFix dwafx = new doWaFix();
		CheckDueTime cdt = new CheckDueTime();
		try {
			String selectIncrease = "SELECT Key, Status, Assignee, Due from " + tableName + filter;
			ResultSet rs1 = stmt.executeQuery(selectIncrease);
			while (rs1.next()) {
				String sResult = rs1.getString("Key");

				String sResult2 = rs1.getString("Status");
				String sAssignee = rs1.getString("Assignee");
				String sDevDueDate = rs1.getString("Due");
				
				if (sResult2.equals("OPEN")) {
					dwaf.doWaFixing(filter, stmt, username, password, sResult);
					dwafx.doWaFix(filter, stmt, username, password, sResult);
				}
				if (sResult2.equals("WORKAROUND FIXING")) {
					dwar.doWAResolved(filter, stmt, username, password, sResult, sAssignee);
				}
				if (sResult2.equals("FIXING BUG") && 1==2) {
					cdt.checkDueTime(sDevDueDate, filter, stmt, username, password, sResult);
				}

			}
		} catch (Exception e) {
			System.out.println("openToWA" + e.getMessage());
			sm.sendMailError("openToWA" + e.toString());
		}
	}
}
