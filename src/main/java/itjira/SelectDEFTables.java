package itjira;

import java.sql.ResultSet;
import java.sql.Statement;

public class SelectDEFTables {

	public void selectTable(String[][] dataSet, int i, int headerColumnNo, String filter, Statement stmt,
			int defColumn, int[] usingCounts, int dueColumn, int assigneeColumn,int summaryColumn,int statusColumn) {

		// includeDefect =

		selectInsertTable(dataSet, i, headerColumnNo, filter, stmt, defColumn, usingCounts, dueColumn, assigneeColumn,summaryColumn,statusColumn); // insert
		// edilecek
		// yeni
		// defectlerin
		// hesaplanmasi

	}

	public void selectInsertTable(String[][] dataSet, int i, int headerColumnNo, String filter, Statement stmt,
			int defColumn, int[] usingCounts, int dueColumn, int assigneeColumn,int summaryColumn,int statusColumn) {
		String tableName = "TEMP_DEFT_";
		String checkData = dataSet[i][defColumn];

		InsertDeleteDEFTables Ins = new InsertDeleteDEFTables();
		SendMail sm = new SendMail();

		try {
			String sMakeSelect = "SELECT COUNT(Key) from " + tableName + filter + " WHERE Key ='" + checkData + "'";
			ResultSet rs = stmt.executeQuery(sMakeSelect);
			String sResult = rs.getString("COUNT(Key)");
			sResult = sResult + "";
			if (sResult.equals("0")) {// Yeni gelen defectler
				int update_icolumn = 0;
				int update_dcolumn = 1;
				Ins.insertDiffDefects(dataSet, i, headerColumnNo, filter, stmt, defColumn, update_icolumn,
						update_dcolumn, usingCounts, dueColumn, assigneeColumn,summaryColumn,statusColumn);
			} else {// Zaten var olan defectler
				int update_icolumn = 1;
				int update_dcolumn = 1;
				Ins.DeleteDiffDefects(dataSet, i, headerColumnNo, filter, stmt, defColumn, update_icolumn,
						update_dcolumn, usingCounts, dueColumn, assigneeColumn,summaryColumn,statusColumn);
			}
		} catch (Exception e) {
			System.out.println("selectInsertTable e:" + e.getMessage());
			sm.sendMailError("selectInsertTable e:" + e.toString());
		}
	}
}
