package itjira;

import java.io.File;
import java.io.PrintWriter;
import java.sql.Statement;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.steadystate.css.parser.ParseException;

public class doWaFix {
	public void doWaFix(String filter, Statement stmt, String username, String password, String sResult)
			throws ParseException {
		SendMail sm = new SendMail();
		try {

			String exePath = null;
			FindPathWithOSName findOS = new FindPathWithOSName();
			exePath = findOS.findOsName();
			System.setProperty("webdriver.chrome.driver", exePath);
			ChromeOptions options = new ChromeOptions();
			
			
			
			options.addArguments("--disable-dev-shm-usage"); // overcome limited
																// resource
																// problems
			options.addArguments("--no-sandbox"); // Bypass OS security model
			options.addArguments("--headless"); // Bypass OS security model
			options.addArguments("--disable-extensions");
			options.addArguments("--disable-gpu");
			
			
			
			WebDriver driver = new ChromeDriver(options);
			WebDriverWait wait = new WebDriverWait(driver, 10);

			driver.get("https://itsupport.vodafone.com.tr/browse/" + sResult + "?filter=" + filter + "&os_username="
					+ username + "&os_password=" + password);
			
			driver.manage().window().setPosition(new Point(0,0));
			driver.manage().window().setSize(new Dimension(1200,768));

			PrintWriter writer = new PrintWriter("the-file-name.html", "UTF-8");
			writer.println(driver.getPageSource());
			writer.close();
	        
			
			WebElement Priorty = driver.findElement(By.xpath("//*[@id='priority-val']/img"));

			String title = Priorty.getAttribute("title");

			title = title.substring(0, 1);
			
			//1 if (!title.equalsIgnoreCase("l"))
			//1 {
	        /*WebElement popUp = wait
					.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='aui-flag-container']/div/div/span")));*/
	        
	        WebElement taskType = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\'type-val\']/img")));
	     
			String type = taskType.getAttribute("title");
			
			type = type.substring(0, 1);
			
	        if(!type.equalsIgnoreCase("p")) {
	        WebElement waFixing = driver.findElement(By.xpath("//*[@id='action_id_2931']/span"));
	        waFixing.click();
	        }else {
	        	//WebElement waFixing = driver.findElement(By.xpath("//*[@id=\'opsbar-opsbar-transitions\']/li[2]/a/span"));
	        	WebElement waFixing = driver.findElement(By.xpath("//*[@id=\'action_id_3171\']/span"));
		        waFixing.click();
		        WebElement waelement = wait
						.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='customfield_14870']")));

				waelement.sendKeys(username);
	        }
			

			

			WebElement waFixingSend = wait.until(
					ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='issue-workflow-transition-submit']")));
			waFixingSend.click();
			//1 }

			 driver.close();
			//driver.quit();
		} catch (Exception e) {
			System.out.println(e.toString());
			sm.sendMailError("doWaFix :"+ " defect id :" + sResult+ " "  + e.toString());

		}

	}

}
