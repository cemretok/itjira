package itjira;

import java.sql.Statement;

public class main {
	final static String DEF = "DEF";

	public static void main(String[] args) {

		/*
		 * START $1 MAIN JSonParser ile config dosyas�nda olu�turdu�umuz aboneleri
		 * �ekilir*/
		//,"79634","79487"
		
		try {
		String[] filters = { "78642","77585","79486","79027"};
		//String[] filters = { "78642"};

		for (int sayac = 0; sayac < filters.length; sayac++) {
			JSonParser jp = new JSonParser();
			jp.getUserInfo(filters[sayac]); // args[0]
			String username = jp.getUsername();
			String password = jp.getPassword();
			String domain = jp.getDomain();
			String from = jp.getFrom();
			String to = jp.getTo();
			String filter = jp.getFilterId();
			String type = jp.getType();

			/*
			 * END $1 MAIN
			 */

			/*
			 * START $2 MAIN Jira'ya connect olup filtrelenmi� defectler �ekilir. Ayn�
			 * zamanda Key, Summary, Assignee gibi headerlari �ekilir.
			 * 
			 */

			JiraLogin jl = new JiraLogin();
			jl.connectJira(username, password, filter);
			int rowNumber = jl.getRows_count(); // defect satir sayisi
			int columnNumber = jl.getColumns_count(); // defect kolon sayisi
			int headerColumnNo = jl.getHeaderColumn_count(); // header kolon sayisi
																// = defect kolon
																// sayisi
			String[] jiraHeaders = jl.getJiraHeaders(); // jira Header bilgisi
			String[][] dataSet = jl.getDataSet(); // jira defectlerinin i�eri�i
			int[] usingCounts = jl.getUsingColumns();
			String logOutToken = jl.logOutCookie();

			if (!jl.getGotError()) {
				/*
				 * END $2 MAIN
				 */

				/*
				 * START $3 MAIN Filtre'ye g�re DEFECT_LOG_<FILTERID> tablosu create edilir /
				 */

				Statement stmt = null;
				CreateTableSQLite SQLite = new CreateTableSQLite();

				try {
					stmt = SQLite.createTable(jiraHeaders, headerColumnNo, filter);
				
				/*
				 * END $3 MAIN
				 */
				/*
				 * START $4 DEFECT_ID tutulan kolon numaras� �ekilir. Assumption : Her filtrede
				 * DEFECT_ID bulunmas� gerekiyor.
				 */
				int defColumn = defIdColumnNumber(jiraHeaders, headerColumnNo);
				int assigneeColumn = assigneeColumnNumber(jiraHeaders, headerColumnNo);
				int summaryColumn = summaryColumnNumber(jiraHeaders, headerColumnNo);
				int statusColumn = statusColumnNumber(jiraHeaders, headerColumnNo);
				int dueColumn = dueDateIdColumnNumber(jiraHeaders, headerColumnNo);

				/*
				 * END $4 MAIN
				 */

				/*
				 * START $5 MAIN Filtre'ye g�re DEFECT_LOG_<FILTERID> tablosuna select �ek /
				 */

				String checkData = null;
				int update_icolumn = 0, update_dcolumn = 0;
				SelectDEFTables Select = new SelectDEFTables();
				for (int i = 0; i < rowNumber; i++) {
					checkData = dataSet[i][defColumn];

					Select.selectInsertTable(dataSet, i, headerColumnNo, filter, stmt, defColumn, usingCounts,
							dueColumn, assigneeColumn, summaryColumn, statusColumn);
				}

				DeleteTableDecreaseDef dtdd = new DeleteTableDecreaseDef();
				String diBody = dtdd.deleteDecreaseDef(username, password, filter, stmt);

				if (type.equals(DEF)) {
					if (diBody.contains("Increase Defect") || diBody.contains("Decrease Defect"))  {
						SendMail sm = new SendMail();
						sm.sendMailJira(to, from, domain, diBody);
					}

					OpenToWAFixing owaf = new OpenToWAFixing();
					owaf.openToWA(filter, stmt, username, password);

				}

				ResetAllColumns rac = new ResetAllColumns();
				rac.resetColumns(filter, stmt);
				
				stmt.close();
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(e.getMessage());
				}

			}
			
			System.out.println("Disconnection is starting with token: " + jl.logOutCookie);
			jl.disconnectJira(username, password, filter);
			System.out.println(domain + " finished");
			
		}
		
		} catch (Exception e) {
			System.out.println(e.toString());
			SendMail sm = new SendMail();
			sm.sendMailError(" main :" + e.toString());

		}


	}

	private static int defIdColumnNumber(String[] jiraHeaders, int headerColumnNo) {

		String defColumnName = "Key";
		for (int i = 0; i < headerColumnNo; i++) {
			if (jiraHeaders[i].equals(defColumnName)) {
				return i;
			}
		}
		return 0;
	}

	private static int assigneeColumnNumber(String[] jiraHeaders, int headerColumnNo) {

		String assigneeColumnName = "Assignee";
		for (int i = 0; i < headerColumnNo; i++) {
			if (jiraHeaders[i].equals(assigneeColumnName)) {
				return i;
			}
		}
		return 0;
	}

	private static int summaryColumnNumber(String[] jiraHeaders, int headerColumnNo) {

		String summaryColumnName = "Summary";
		for (int i = 0; i < headerColumnNo; i++) {
			if (jiraHeaders[i].equals(summaryColumnName)) {
				return i;
			}
		}
		return 0;
	}

	private static int statusColumnNumber(String[] jiraHeaders, int headerColumnNo) {

		String statusColumnName = "Status";
		for (int i = 0; i < headerColumnNo; i++) {
			if (jiraHeaders[i].equals(statusColumnName)) {
				return i;
			}
		}
		return 0;
	}

	private static int dueDateIdColumnNumber(String[] jiraHeaders, int headerColumnNo) {

		String dueDateColumnName = "Due";
		for (int i = 0; i < headerColumnNo; i++) {
			if (jiraHeaders[i].equals(dueDateColumnName)) {
				return i;
			}
		}
		return 0;
	}

}
